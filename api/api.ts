import {HttpClient} from 'aurelia-fetch-client';
import {inject} from 'aurelia-framework';

@inject(HttpClient)
export class Api {
	host = '/api';

 	constructor(httpClient : HttpClient){
 		this.client = httpClient;
 	}


 	request({ method, path, body }) : Promise{
    return this.client.fetch(`${this.host}${path}`, {
      headers: {
        'Content-Type': 'application/json'
      },
      method,
      body: JSON.stringify(body)
    }).then((response) => {
    		return response.json();
    });
  }
}
