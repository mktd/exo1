import {Api} from './api/api';
import {inject} from 'aurelia-framework';

@inject(Api)
export class App {
  message = 'Hello Brendish!';
  username: String;
  data: {};

 	constructor(api : Api){
 		this.apiService = api;
 	}


  defineQuizUsername(): void {
    const path = `/quizz?userName=${this.username}`
    this.apiService.request({method:'get',path}).then(data => {
    	console.log(data);
    	this.data = data;
    	this.username = '';
    });
  }
}
